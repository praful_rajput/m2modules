<?php
/**
 *
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @copyright   Copyright (c) 2014 X.commerce, Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Ktpl\Bannerslider\Controller\Adminhtml\Manage;

use Ktpl\Bannerslider\Controller\Adminhtml\Banner as BannerController;
use Magento\Framework\Registry;
use Ktpl\Bannerslider\Model\BannersliderFactory;
use Magento\Backend\Model\Session as BackendSession;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\View\Result\PageFactory;


class Edit extends BannerController
{
    /**
     * backend session
     *
     * @var BackendSession
     */
    protected $backendSession;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * constructor
     *
     * @param Registry $registry
     * @param BannersliderFactory $bannerFactory
     * @param BackendSession $backendSession
     * @param PageFactory $resultPageFactory
     * @param Context $context
     * @param RedirectFactory $resultRedirectFactory
     */
    public function __construct(
        Registry $registry,
        PageFactory $resultPageFactory,
        BannersliderFactory $bannerFactory,
        BackendSession $backendSession,
        RedirectFactory $resultRedirectFactory,
        Context $context

    )
    {
        $this->backendSession = $backendSession;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($registry, $bannerFactory, $resultRedirectFactory, $context);
    }
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {


        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('banner_id');
        /** @var \Ktpl\Bannerslider\Model\Bannerslider $banner*/
        $banner = $this->initBanner();

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Ktpl_Bannerslider::banner');
        $resultPage->getConfig()->getTitle()->set((__('Banner')));

        // 2. Initial checking
        if ($id) {
            $banner->load($id);
            if (!$banner->getId()) {
                $this->messageManager->addError(__('This banner no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath(
                    'ktpl_bannerslider/*/edit',
                    [
                        'banner_id' => $banner->getId(),
                        '_current' => true
                    ]
                );
                return $resultRedirect;
            }
        }

        // 3. Set entered data if was error when we do save

        $title = $banner->getId() ? $banner->getName() : __('New Banner');
        $resultPage->getConfig()->getTitle()->append($title);
        $data = $this->backendSession->getData('ktpl_bannerslider_banner_data', true);
        if (!empty($data)) {
            $banner->setData($data);
        }
        return $resultPage;

    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Ktpl_Bannerslider::save');
    }
}
