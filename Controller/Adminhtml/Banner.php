<?php
namespace Ktpl\Bannerslider\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Ktpl\Bannerslider\Model\BannersliderFactory;
use Magento\Framework\Registry;


abstract class Banner extends Action
{
    /**
     * inquiry factory
     *
     * @var AuthorFactory
     */
    protected $bannerFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * date filter
     *
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date
     */
    protected $dateFilter;

    /**
     * @param Registry $registry
     * @param AuthorFactory $bannerFactory
     * @param RedirectFactory $resultRedirectFactory
     * @param Context $context
     */
    public function __construct(
        Registry $registry,
        BannersliderFactory $bannerFactory,
        RedirectFactory $resultRedirectFactory,
        Context $context

    )
    {
        $this->coreRegistry = $registry;
        $this->bannerFactory = $bannerFactory;
        $this->resultRedirectFactory = $resultRedirectFactory;
        parent::__construct($context);
    }

    /**
     * @return \Sample\News\Model\Author
     */
    protected function initBanner()
    {
        $bannerId  = (int) $this->getRequest()->getParam('banner_id');
        /** @var \Ktpl\Inquiry\Model\Inquiry $banner */
        $banner    = $this->bannerFactory->create();
        if ($bannerId) {
            $banner->load($bannerId);
        }
        $this->coreRegistry->register('ktpl_bannerslider_banner', $banner);
        return $banner;
    }



}
