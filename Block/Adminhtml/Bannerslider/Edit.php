<?php
namespace Ktpl\Bannerslider\Block\Adminhtml\Bannerslider;

use Magento\Backend\Block\Widget\Form\Container as FormContainer;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Edit extends FormContainer
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * constructor
     *
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    )
    {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize inquiry edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Ktpl_Bannerslider';
        $this->_controller = 'adminhtml_bannerslider';
        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save Block'));
        // $this->buttonList->update('delete', 'label', __('Delete Block'));
        $this->buttonList->add(
            'saveandcontinue',
            array(
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => array(
                    'mage-init' => array('button' => array('event' => 'saveAndContinueEdit', 'target' => '#edit_form'))
                )
            ),
            -100
        );

        $this->buttonList->update('delete', 'label', __('Delete Author'));

    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        $inquiry = $this->coreRegistry->registry('ktpl_inquiry_inquiry');
        if ($inquiry->getId()) {
            return __("Edit Item '%1'", $this->escapeHtml($inquiry->getTitle()));
        } else {
            return __('New Banner');
        }
    }

    /**
     * Prepare layout
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('inquiry_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'inquiry_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'inquiry_content');
                }
            };
        ";
        return parent::_prepareLayout();
    }
}
