<?php
namespace Ktpl\Bannerslider\Api\Data;


interface BannersliderInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const INQUIRY_ID  = 'banner_id';
    const FIRST_NAME  = 'first_name';
    const LAST_NAME   = 'last_name';
    const ADDRESS     = 'address';
    const PHONE       = 'phone';
    const EMAIL       = 'email';
    const COMMENTS    = 'comments';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get First Name
     *
     * @return string
     */
    public function getFirstName();

    /**
     * Get Last Name
     *
     * @return string|null
     */
    public function getLastName();

    /**
     * Get Address
     *
     * @return string|null
     */
    public function getAddress();

    /**
     * Get Email
     *
     * @return string|null
     */
    public function getEmail();



    /**
     * Set ID
     *
     * @param int $id
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     */
    public function setId($id);

    /**
     * Set URL Key
     *
     * @param string $url_key
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     */
    public function setFirstName($first_name);

    /**
     * Set title
     *
     * @param string $title
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     */
    public function setLastName($last_name);

    /**
     * Set content
     *
     * @param string $content
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     */
    public function setAddress($address);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \Ashsmith\Blog\Api\Data\PostInterface
     */
    public function setEmail($email);

}
