/**
 * Created by praful.rajput on 10/9/2015.
 */
/**
 * Copyright � 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            bannerslider: 'Ktpl_Bannerslider/js/flexslider'
        }
    },
    shim: {
        'bannerslider': {
            deps: ['jquery']
        }
    }
};

